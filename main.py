#!/bin/python

import socket
import time
import numpy as np
import itertools
import pprint
import sys
import traceback
import random

HOST = "94.45.241.27"
PORT = 4000

with open("/home/volker/Sync/DatenVolker/passphrase.txt") as pfile:
    passphrase = pfile.read().lstrip().rstrip()


def parsesingle(servermsg):
    servermsg = servermsg.lstrip().rstrip()
    if servermsg == "":
        return None
    ret = servermsg.split("|")
    for i in range(len(ret)):
        try:
            ret[i] = int(ret[i])
        except:
            pass
    return ret


def parse(servermsg):
    it = filter(None, map(parsesingle, servermsg.split("\n")))
    return {el[0]: el[1:] for el in it}


mapsize = 20
# coordshift = np.array([3, 3])
coordshift = np.array([0, 0])
visited = np.full((mapsize, mapsize), False, dtype=object)
goodness = np.full((mapsize, mapsize), 0, dtype=object)
walls_horizontal = np.full((mapsize, mapsize), 1)
walls_vertical = np.full((mapsize, mapsize), 1)

# todo: should the size be mapsize+1 or mapsize-1 ?
walls = [np.full((mapsize, mapsize), 1), np.full((mapsize, mapsize), 1)]

# todo: flipping horizontal and vertical should be easy
left = np.array([-1, 0])
right = np.array([+1, 0])
up = np.array([0, -1])
down = np.array([0, +1])

horizontal = 0
vertical = 1

large = 999  # todo: I don't like this


def calc_wall_inds(xy, direct):
    if np.array_equal(direct, left):
        return (horizontal, tuple(xy))
    elif np.array_equal(direct, right):
        return (horizontal, tuple(xy + right))
    elif np.array_equal(direct, up):
        return (vertical, tuple(xy))
    elif np.array_equal(direct, down):
        return (vertical, tuple(xy + down))


def is_bad_inds(inds):
    return (
        inds[1][0] < 0
        or inds[1][1] < 0
        or inds[1][0] >= mapsize
        or inds[1][1] >= mapsize
    )


def get_wall(xy, direct):
    inds = calc_wall_inds(xy, direct)
    if is_bad_inds(inds):
        return 1
    return walls[inds[0]][inds[1]]


def clear_wall(xy, direct):
    inds = calc_wall_inds(xy, direct)
    if is_bad_inds(inds):
        return
    walls[inds[0]][inds[1]] = 0


def ascii_art(goodness, curpos):
    for y in range(mapsize):
        for x in range(mapsize):
            if get_wall([x, y], left) == 0:
                print("-", end="")
            else:
                print(" ", end="")
            if (x, y) == tuple(curpos - coordshift):
                print("c", end="")
            elif (x, y) == tuple(np.array([18, 18]) - coordshift):
                print("x", end="")
            else:
                print(goodness[x, y], end="")
        print("")

        for x in range(mapsize):
            print(" ", end="")
            if get_wall([x, y], down) == 0:
                print("|", end="")
            else:
                print(" ", end="")

        print("")


dirs = [
    (0, left, left, "left"),
    (0, right, np.array([0, 0]), "right"),
    (1, up, up, "up"),
    (1, down, np.array([0, 0]), "down"),
]


class Solver:
    def step(self, answers, goal):
        print("step arg:", answers, goal)
        pos = answers["pos"]
        curpos = (
            np.array([pos[0], pos[1]]) + coordshift
        )  # Because the indexes cannot be negative
        goal += coordshift
        visited[tuple(curpos)] = True
        if pos[2] == 0:
            clear_wall(curpos, up)
        if pos[3] == 0:
            clear_wall(curpos, right)
        if pos[4] == 0:
            clear_wall(curpos, down)
        if pos[5] == 0:
            clear_wall(curpos, left)
        # print(get_wall([1, 5], left))

        goodness.fill(large)
        for x in range(mapsize):
            for y in range(mapsize):
                if visited[x, y]:
                    goodness[x, y] = 0

        stale = False
        while not stale:
            stale = True
            for x in range(mapsize):
                for y in range(mapsize):
                    xy = [x, y]
                    for d in dirs:
                        axis, direct, offs, name = d

                        if get_wall(xy, direct) == 0:  # If there is no wall
                            newval = max(
                                goodness[tuple(xy)], goodness[tuple(xy + direct)] - 1
                            )
                            if newval != goodness[tuple(xy)]:
                                stale = False
                                goodness[tuple(xy)] = newval
        # ascii_art(goodness, curpos)

        bestgoodness = 0
        bestdot = 0
        bestdir = None
        for d in dirs:
            axis, direct, offs, name = d
            if get_wall(curpos, direct) == 0:
                temp = goodness[tuple(curpos + direct)]
                tempdot = np.dot(direct, goal - curpos)
                if temp > bestgoodness or temp == bestgoodness and tempdot > bestdot:
                    bestgoodness = temp
                    bestdir = name
                    bestdot = tempdot
        assert bestdir is not None
        print("step returns", bestdir)
        return bestdir


def simulate():
    solver = Solver()
    lastlog = [
        {"pos": [17, 17, 0, 1, 1, 1]},
        {"pos": [17, 16, 0, 1, 0, 0]},
        {"pos": [17, 15, 0, 1, 0, 1]},
        {"pos": [17, 14, 1, 1, 0, 1]},
    ]
    goal = [0, 0]
    for i in range(len(lastlog)):
        solver.step(lastlog[i], goal)


def sendall(s, str):
    print("raw send:", str)
    s.sendall(str.encode("utf-8"))


def recv(s):
    data = s.recv(1024).decode("utf-8")
    # if "goal" in data:
    #     while not "pos" in data or data[-1] != "\n":
    #         data += s.recv(1024).decode("utf-8")
    print("raw receive", data)
    return data


def client():
    while True:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
            # time.sleep(0.1)
            s.connect((HOST, PORT))
            # time.sleep(0.1)
            sendall(s, "join|Volker|" + passphrase + "\n")

            data = recv(s)

            answers = parse(recv(s))
            goal = answers["goal"]
            print("goal:", goal)

            del answers["goal"]
            solver = Solver()
            log = ""
            steplog = ""

            try:
                print("------------------------------------")
                while (
                    "goal" not in answers
                    and "lose" not in answers
                    and "win" not in answers
                ):
                    log += str(answers) + ","
                    bestdir = solver.step(answers, goal)
                    steplog += bestdir + "\n"
                    sendall(s, "move|" + bestdir + "\n")
                    answers = parse(recv(s))
                    # while "error" in answers:
                    #     print("error:", answers)
                    #     sendall(
                    #         s,
                    #         "move|"
                    #         + ["up", "down", "left", "right"][random.randint(0, 3)]
                    #         + "\n",
                    #     )
                    #     answers = parse(recv(s))
                    sys.stdout.flush()
            except Exception:

                print(traceback.print_exc())
                print("####################################")
                print(steplog)
                print("[", log, "]")
                print(answers)
                exit(0)


def naughty():
    with open("blns.txt", "r") as f:
        # put all lines in the file into a Python list
        content = f.readlines()

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        sendall(s, "join|Volker|" + passphrase + "\n")

        while True:
            sendall(
                s,
                "move|" + ["up", "down", "left", "right"][random.randint(0, 3)] + "\n",
            )
            # data = s.recv(1024).decode("utf-8")
            # print(data)
            naughty = content[random.randint(0, len(content) - 1)]
            sendall(s, "chat|" + naughty)
            time.sleep(5.5)


# simulate()
client()
# naughty()

#
# {'pos': [0, 5, 0, 1, 1, 1]}
